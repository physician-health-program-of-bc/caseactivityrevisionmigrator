<?php
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2: */

require_once 'caseactivityrevisionmigrator.civix.php';
use CRM_Caseactivityrevisionmigrator_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function caseactivityrevisionmigrator_civicrm_config(&$config) {
  _caseactivityrevisionmigrator_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function caseactivityrevisionmigrator_civicrm_xmlMenu(&$files) {
  _caseactivityrevisionmigrator_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function caseactivityrevisionmigrator_civicrm_install() {
  _caseactivityrevisionmigrator_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function caseactivityrevisionmigrator_civicrm_postInstall() {
  _caseactivityrevisionmigrator_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function caseactivityrevisionmigrator_civicrm_uninstall() {
  _caseactivityrevisionmigrator_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function caseactivityrevisionmigrator_civicrm_enable() {
  _caseactivityrevisionmigrator_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function caseactivityrevisionmigrator_civicrm_disable() {
  _caseactivityrevisionmigrator_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function caseactivityrevisionmigrator_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _caseactivityrevisionmigrator_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function caseactivityrevisionmigrator_civicrm_managed(&$entities) {
  _caseactivityrevisionmigrator_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function caseactivityrevisionmigrator_civicrm_caseTypes(&$caseTypes) {
  _caseactivityrevisionmigrator_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function caseactivityrevisionmigrator_civicrm_angularModules(&$angularModules) {
  _caseactivityrevisionmigrator_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function caseactivityrevisionmigrator_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _caseactivityrevisionmigrator_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function caseactivityrevisionmigrator_civicrm_entityTypes(&$entityTypes) {
  _caseactivityrevisionmigrator_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_thems().
 */
function caseactivityrevisionmigrator_civicrm_themes(&$themes) {
  _caseactivityrevisionmigrator_civix_civicrm_themes($themes);
}

/**
 * hook_civicrm_navigationMenu()
 */
function caseactivityrevisionmigrator_civicrm_navigationMenu(&$menu) {
  _caseactivityrevisionmigrator_civix_insert_navigation_menu($menu, 'Administer/CiviCase', array(
    'label' => E::ts('Revision Migrator'),
    'name' => 'caseactivityrevisionmigrator_intro',
    'url' => 'civicrm/caseactivityrevisionmigrator/intro?reset=1',
    'permission' => 'administer CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _caseactivityrevisionmigrator_civix_navigationMenu($menu);
}
