<?php
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2: */

// phpcs:disable
eval(`cv php:boot`);
// phpcs:enable

// Check that the server even supports trigger-based logging.
// This will throw an exception if it doesn't, so let it do that.
require_once 'api/Exception.php';
$true_value_that_needs_to_be_an_actual_variable = TRUE;
CRM_Logging_Schema::checkLoggingSupport($true_value_that_needs_to_be_an_actual_variable, array());

$args = getopt('kd:', array('keep-old-revisions', 'debug-level:'));
$options = array();
if (isset($args['k']) || isset($args['keep-old-revisions'])) {
  $options['delete_old_revisions'] = FALSE;
}
if (isset($args['d']) || isset($args['debug-level'])) {
  $options['debug_level'] = isset($args['d']) ? $args['d'] : $args['debug-level'];
}
$m = new CRM_Caseactivityrevisionmigrator_Migrator($options);
$m->setup();
$m->run();
