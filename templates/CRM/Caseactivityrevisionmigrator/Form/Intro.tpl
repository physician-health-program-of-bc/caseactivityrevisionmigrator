{crmScope extensionKey='caseactivityrevisionmigrator'}
<div class="help">
  {ts}Case Activity Revision Migrator Settings{/ts}
</div>

<div class="crm-block crm-form-block crm-component-form-block">
{foreach from=$elementNames item=elementName}
  <div class="crm-section">
    <div class="label">{$form.$elementName.label}</div>
    <div class="content">{$form.$elementName.html}
    {if $elementName == "keep_noncurrent_revisions"}
      <div class="description">{ts}You can choose to keep the old non-current revisions in the database instead of letting it delete them after, but an alternate option would be to make a backup first of your database for archive purposes.{/ts}</div>
    {/if}
    </div>
    <div class="clear"></div>
  </div>
{/foreach}
</div>

<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="bottom"}
</div>
{/crmScope}
