# caseactivityrevisionmigrator

Provides options and tools to migrate from activity revisions (Administer - CiviCase - CiviCase Settings - Enable Embedded Activity Revisions) to trigger-based logging (Administer - System Settings - Misc - Logging).

The extension is licensed under [MIT](LICENSE.txt).

## FAQ

1. Who is this for?

   Case activity revisions will eventually be deprecated in favor of trigger-based logging, and for a while now have only worked for activities manipulated by the API or customizations and not in the UI. If you have been using case activity revisions and still want to be able to see the old revisions once it is fully deprecated, you will need to convert them to trigger-based log records.

   An alternative would be to keep a separate read-only archive installation available to view that history, and then just use trigger-based logging in your live system going forward, in which case you don't need this extension.

   You also don't need this extension if you have only been using the core UI and your install is only a few years old, since revisions have been broken in the core UI for a while so this extension won't be able to make proper log records out of the data anyway. It will only really work on older records or records updated via API or customized UI.

1. I've run it and then went to the Contact Logging CiviReport but it doesn't seem to correctly show the history.

   This is a known issue. The *Contact* logging report does not always show the right thing for *non-contact* records. It will need to be addressed but the focus of this extension is creating the correct records in the database.

1. Also I can't see the history on the activity, like the way it shows the change log on contact records.

   Yep that too. The change log on contact records when logging is enabled is technically the same report as above.

1. I'm waiting for it to finish but it doesn't seem to be doing anything.

   When running via the UI I have no idea how to send anything back to the queue screen for display. It will however, for both the UI and command line, periodically log a progress update to the standard ConfigAndLog, so you can check the log file in that folder.

1. I already had logging turned on - can I find just the records this made?

   Yes. They will all have the same two-letter prefix in the log_conn_id field in the log_XXX tables.

## Requirements

* PHP v7.0+
* CiviCRM 5.23+

## Installation (Web UI)

This extension has not yet been published for installation via the web UI.

## Installation (CLI, Zip)

Sysadmins and developers may download the `.zip` file for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
cd <extension-dir>
cv dl https://lab.civicrm.org/extensions/caseactivityrevisionmigrator/-/archive/master/caseactivityrevisionmigrator-master.zip
```

## Installation (CLI, Git)

Sysadmins and developers may clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and
install it with the command-line tool [cv](https://github.com/civicrm/cv).

```bash
git clone https://lab.civicrm.org/extensions/caseactivityrevisionmigrator.git
cv en caseactivityrevisionmigrator
```

## Usage

* For UI: Go to Administer - CiviCase - Revision Migrator

* For command line, `cv` needs to be installed. From inside this extension's folder run `php cli.php`.
  * -k|--keep-old-revisions: Don't delete the non-current revisions when finished.
  * -d|--debug_level:
    * 0: Default
    * 1: Interesting
  * Example: `php cli.php -k -d=1`

* If you chose to delete the old non-current revisions (i.e. didn't use the -k option) you may want to run [OPTIMIZE TABLE](https://dev.mysql.com/doc/refman/5.7/en/optimize-table.html) on the civicrm_activity table and possibly other tables like civicrm_activity_contact, civicrm_case_activity, civicrm_entity_tag, and related civicrm_custom_value_XXX tables after the migration. This will reclaim space and reorganize the indexes, however depending on your database this may be a very heavy operation.

* If it's not obvious, this will turn on trigger-based logging if you didn't have it on before. It will also turn off case activity revisions. You can run with both turned on, so you can turn case activity revisions back on if you like.

## Known Issues

If left installed and you step out for too long it will start selling your belongings on ebay.

The strategy used to avoid creating duplicates if logging was ever turned on already may miss some revisions if logging was turned on and off and on again over time during the life of the site.

The algorithm may not be very efficient for very large databases.

As noted in the FAQ the existing *Contact* Logging Report does not currently properly display activities and some other non-contact records. You will need to look in the database to verify it has done the right thing.

The "Initialization" entries that the logging system creates for every single record in the database when you turn on logging don't necessarily make sense for activities with revisions. They take up space and might be confusing, but don't prevent the script from running.

There are some types of records where it's some work to evaluate the changes under the revision system. For example the situation where an activity assignee was removed. The algorithm would need to compare the
assignees against the assignees of the previous revision id. I've left this out for now.

Because revisions have not been working in the core UI for some time, updates to activities that were made in the core UI will lead to spurious Insert entries in the log_XXX tables, since the algorithm assumes revisions are working. So if you have a mix of custom code that is working, but have also been making updates in the core UI, you'll end up with some mixed log records.

## TODO

Think about custom fields of type file - does it need anything special?
