<?php
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2: */

/**
 * Internal tool to help with manual testing by creating enough revisions of
 * existing case activities to make for a useful sample.
 * Can run with `cv php:script`.
 *
 * TODO: Want to test revisions to custom fields too. While this will create
 * revisions of custom fields, this script is only making actual changes to
 * core activity fields.
 */


// How many to make
define('NUM_REVISIONS', 100);

// Probability that we should make another revision of the same activity we
// just did (out of 100 percent).
define('MULTIPLE_PER_ACTIVITY_PCT', 5);

// Make sure revisions are turned on.
$old_revision_setting = Civi::settings()->get('civicaseActivityRevisions');
Civi::settings()->set('civicaseActivityRevisions', '1');

// Get some random case activities.
$sql_params = array(1 => array(NUM_REVISIONS, 'Integer'));
$dao = CRM_Core_DAO::executeQuery("
  SELECT
  ca.case_id,
  a.id,
  a.status_id,
  a.subject,
  a.details
  FROM civicrm_case_activity ca
  INNER JOIN civicrm_activity a ON ca.activity_id = a.id
  WHERE a.is_current_revision = 1
  AND a.is_deleted = 0
  ORDER BY RAND()
  LIMIT %1
  ", $sql_params);
while ($dao->fetch()) {
  // copy the values in case we repeat for the same activity
  $values = array(
    'id' => $dao->id,
    'case_id' => $dao->case_id,
    'subject' => $dao->subject,
    'status_id' => $dao->status_id,
    'details' => $dao->details,
  );
  do {
    $is_dirty = FALSE;
    $update_params = array(
      'id' => $values['id'],
      'case_id' => $values['case_id'],
    );
    // Anecdotally, details and status changes are the most common.
    // Although status has a progression. Let's just guess some things.
    while (!$is_dirty) {
      if (mt_rand(1, 100) < 50) {
        $update_params['details'] = $values['details'] . ' aaa';
        $values['details'] = $update_params['details'];
        $is_dirty = TRUE;
      }
      if (mt_rand(1, 100) < 20) {
        $update_params['subject'] = $values['subject'] . ' bbb';
        $values['subject'] = $update_params['subject'];
        $is_dirty = TRUE;
      }
      if (mt_rand(1, 100) < 10) {
        $update_params['status_id'] = 'Completed';
        $values['status_id'] = $update_params['status_id'];
        $is_dirty = TRUE;
      }
    }
    echo "Updating activity id: {$values['id']}, case id: {$values['case_id']}\n";
    $result = civicrm_api3('Activity', 'create', $update_params);

    // Get the new revision id
    $values['id'] = $result['id'];

    $repeat_same_activity = (mt_rand(1, 100) <= MULTIPLE_PER_ACTIVITY_PCT);
  } while ($repeat_same_activity);
}

// replace old setting
Civi::settings()->set('civicaseActivityRevisions', $old_revision_setting);
