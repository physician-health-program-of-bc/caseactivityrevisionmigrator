<?php
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2: */

use CRM_Caseactivityrevisionmigrator_ExtensionUtil as E;

/**
 * Queue since task might be long running
 */
class CRM_Caseactivityrevisionmigrator_Page_MigratorQueue extends CRM_Core_Page {
  const QUEUE_NAME = 'migrator-queue';

  public function run() {
    $queue = CRM_Queue_Service::singleton()->create([
      // type Memory would make more sense here but it just hangs after completing the task
      'type' => 'Sql',
      'name' => self::QUEUE_NAME,
      'reset' => TRUE,
    ]);

    $queue->createItem(
      new CRM_Queue_Task(
        array('CRM_Caseactivityrevisionmigrator_Page_MigratorQueue', 'migrateTask'),
        array(),
        E::ts('Migrate Activity Revisions')
      )
    );

    $runner = new CRM_Queue_Runner(
      array(
        'title' => E::ts('Migrator Queue Runner'),
        'queue' => $queue,
        'onEnd' => array('CRM_Caseactivityrevisionmigrator_Page_MigratorQueue', 'onEnd'),
        'onEndUrl' => CRM_Utils_System::url('civicrm/caseactivityrevisionmigrator/intro'),
      )
    );
    $runner->runAllViaWeb();
  }

  /**
   * Do the task
   *
   * @param \CRM_Queue_TaskContext $ctx
   * @return bool
   */
  public static function migrateTask(CRM_Queue_TaskContext $ctx) {
    $session = CRM_Core_Session::singleton();
    $options = $session->get('caseactivityrevisionmigrator_options') ?? array();

    $m = new CRM_Caseactivityrevisionmigrator_Migrator($options);
    $m->setup();
    $m->run();
    CRM_Core_Error::debug_log_message('doing stuff');
    $session->set('caseactivityrevisionmigrator_options', NULL);
    return TRUE;
  }

  /**
   * Handle the final step of the queue
   * @param \CRM_Queue_TaskContext $ctx
   */
  public static function onEnd(CRM_Queue_TaskContext $ctx) {
    CRM_Core_Error::debug_log_message('Migration Done.');
    CRM_Core_Session::setStatus(
      E::ts('Migration complete.'),
      '',
      'success',
      array(
        'expires' => 0,
      )
    );
  }

}
