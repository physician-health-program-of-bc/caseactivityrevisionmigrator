<?php
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2: */

/**
 * This is just copied from CRM_Logging_Schema because I can't use it directly
 * since the function I want is private there.
 */
class CRM_Caseactivityrevisionmigrator_Schema {

  /**
   * Get an array of columns and their details like DATA_TYPE, IS_NULLABLE, COLUMN_DEFAULT for the given table.
   *
   * @param string $table
   *
   * @return array
   */
  public static function columnSpecsOf($table) {
    static $civiDB = NULL;
    if (empty(\Civi::$statics[__CLASS__]['columnSpecs'])) {
      \Civi::$statics[__CLASS__]['columnSpecs'] = [];
    }
    if (empty(\Civi::$statics[__CLASS__]['columnSpecs']) || !isset(\Civi::$statics[__CLASS__]['columnSpecs'][$table])) {
      if (!$civiDB) {
        $dao = new CRM_Contact_DAO_Contact();
        $civiDB = $dao->_database;
      }
      CRM_Core_TemporaryErrorScope::ignoreException();
      // NOTE: W.r.t Performance using one query to find all details and storing in static array is much faster
      // than firing query for every given table.
      $query = "
SELECT TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_DEFAULT, COLUMN_TYPE, EXTRA
FROM   INFORMATION_SCHEMA.COLUMNS
WHERE  table_schema = '{$civiDB}'";
      $dao = CRM_Core_DAO::executeQuery($query);
      if (is_a($dao, 'DB_Error')) {
        return [];
      }
      while ($dao->fetch()) {
        if (!array_key_exists($dao->TABLE_NAME, \Civi::$statics[__CLASS__]['columnSpecs'])) {
          \Civi::$statics[__CLASS__]['columnSpecs'][$dao->TABLE_NAME] = [];
        }
        \Civi::$statics[__CLASS__]['columnSpecs'][$dao->TABLE_NAME][$dao->COLUMN_NAME] = [
          'COLUMN_NAME' => $dao->COLUMN_NAME,
          'DATA_TYPE' => $dao->DATA_TYPE,
          'IS_NULLABLE' => $dao->IS_NULLABLE,
          'COLUMN_DEFAULT' => $dao->COLUMN_DEFAULT,
          'EXTRA' => $dao->EXTRA,
        ];
        if (($first = strpos($dao->COLUMN_TYPE, '(')) != 0) {
          // this extracts the value between parentheses after the column type.
          // it could be the column length, i.e. "int(8)", "decimal(20,2)")
          // or the permitted values of an enum (e.g. "enum('A','B')")
          $parValue = substr(
            $dao->COLUMN_TYPE, $first + 1, strpos($dao->COLUMN_TYPE, ')') - $first - 1
          );
          if (strpos($parValue, "'") === FALSE) {
            // no quote in value means column length
            \Civi::$statics[__CLASS__]['columnSpecs'][$dao->TABLE_NAME][$dao->COLUMN_NAME]['LENGTH'] = $parValue;
          }
          else {
            // single quote means enum permitted values
            \Civi::$statics[__CLASS__]['columnSpecs'][$dao->TABLE_NAME][$dao->COLUMN_NAME]['ENUM_VALUES'] = $parValue;
          }
        }
      }
    }
    return \Civi::$statics[__CLASS__]['columnSpecs'][$table];
  }

}
