<?php
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2: */

use CRM_Caseactivityrevisionmigrator_ExtensionUtil as E;

/**
 * Form to let user pick some settings and start the queue.
 */
class CRM_Caseactivityrevisionmigrator_Form_Intro extends CRM_Core_Form {

  /**
   * Check the server supports logging, same as when you enable logging
   * on the core settings form.
   */
  public function preProcess() {
    parent::preProcess();

    // Do they even have anything to convert?
    $noncurrent_revision = CRM_Core_DAO::singleValueQuery("SELECT id FROM civicrm_activity WHERE is_current_revision = 0 LIMIT 1");
    if (empty($noncurrent_revision)) {
      CRM_Core_Error::statusBounce(E::ts("You don't seem to have ever used case activity revisions. You don't need this extension."));
    }

    // Needs to be an actual variable since pass by ref. Sure.
    $value = TRUE;
    try {
      // Note an error that the core function can't find class API_Exception
      // happens if we don't have this require statement.
      require_once 'api/Exception.php';
      CRM_Logging_Schema::checkLoggingSupport($value, array());
    }
    catch (API_Exception $e) {
      // Note it's already translated. It probably shouldn't be though.
      CRM_Core_Error::statusBounce($e->getMessage());
    }

    // Check multilingual and just give a warning.
    if (CRM_Core_I18n::isMultiLingual()) {
      CRM_Core_Session::setStatus(
        E::ts('Your system is set to multilingual. There are some known issues with trigger-based logging and multilingual. But you may continue anyway.'),
        '',
        'alert',
        array(
          'expires' => 0,
        )
      );
    }
  }

  public function buildQuickForm() {

    $this->addYesNo(
      'keep_noncurrent_revisions',
      E::ts('Keep the old revisions in the database after migrating'),
      FALSE,
      TRUE
    );

    $this->add(
      'select',
      'debug_level',
      E::ts('Debug Level'),
      array(
        '0' => E::ts('Default'),
        '1' => E::ts('Interesting'),
      ),
      TRUE
    );

    $this->addButtons(array(
      array(
        'type' => 'submit',
        'name' => E::ts('Migrate Now'),
        'isDefault' => TRUE,
      ),
    ));

    $this->assign('elementNames', $this->getRenderableElementNames());
    parent::buildQuickForm();
  }

  public function setDefaultValues() {
    return array('keep_noncurrent_revisions' => '0');
  }

  public function postProcess() {
    parent::postProcess();
    $values = $this->exportValues();

    // The whole rest of this function seems awkward.
    $options = array(
      'debug_level' => (int) $values['debug_level'],
    );
    if ($values['keep_noncurrent_revisions'] ?? 0) {
      $options['delete_old_revisions'] = FALSE;
    }
    $session = CRM_Core_Session::singleton();
    $session->set('caseactivityrevisionmigrator_options', $options);

    CRM_Utils_System::redirect(CRM_Utils_System::url('civicrm/caseactivityrevisionmigrator/queue'));
  }

  /**
   * Get the fields/elements defined in this form.
   *
   * @return array (string)
   */
  public function getRenderableElementNames() {
    // The _elements list includes some items which should not be
    // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
    // items don't have labels.  We'll identify renderable by filtering on
    // the 'label'.
    $elementNames = array();
    foreach ($this->_elements as $element) {
      /** @var HTML_QuickForm_Element $element */
      $label = $element->getLabel();
      if (!empty($label)) {
        $elementNames[] = $element->getName();
      }
    }
    return $elementNames;
  }

}
