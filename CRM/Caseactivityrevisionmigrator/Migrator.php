<?php
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2: */

/**
 * Backend functions for doing the migration.
 */
class CRM_Caseactivityrevisionmigrator_Migrator {

  /**
   * @var int
   *   Verbosity of log messages.
   *   0 = Default.
   *   1 = Some interesting stuff.
   */
  private $debug_level = 0;

  /**
   * @var CRM_Logging_Schema
   *   Used to enable/disable logging.
   */
  private $logging_schema;

  /**
   * @var string
   *   To provide partial support for alternate logging database.
   */
  private $logging_database = '';

  /**
   * @var string
   *   Not strictly necessary but might be useful someday to pull out all
   *   the entries we made in this run.
   */
  private $conn_prefix;

  /**
   * @var int
   *   We need to put something in the log_conn_id field. We try to keep it
   *   the same for all related records that would have been updated in the
   *   same action, e.g. if you update an activity, it also updates
   *   activity_contacts, entity_tags, etc. If you could search across all
   *   log_XXX tables for a given log_conn_id that would show you everything
   *   updated by a given action.
   *   TODO: It's currently 15 digits max - would we ever run out of numbers?
   *   activity.id itself is only int(10), so seems unlikely?
   */
  private $conn_id;

  /**
   * @var string
   *   If logging was already enabled when was the earliest entry.
   */
  private $earliest_logging_date = NULL;

  /**
   * @var bool
   *   If the old revisions should be removed from the database at the end.
   */
  private $should_delete_old_revisions = TRUE;

  /**
   * @var CRM_Utils_SQL_TempTable
   *   Temporary table to work around civicrm_log duplicates.
   */
  private $nodupes_table;

  /**
   * @var string
   *   The tablename of our temporary table.
   */
  private $nodupes_civicrm_log = '';

  /**
   * Constructor
   *
   * @param array $options
   *   bool delete_old_revisions Whether to delete old revisions at the end.
   */
  public function __construct($options = array()) {
    $this->debug_level = ((int) $options['debug_level']) ?? 0;
    $this->debug(1, "Debug Level: ", $this->debug_level);
    $this->should_delete_old_revisions = $options['delete_old_revisions'] ?? TRUE;
    $this->debug(1, "Delete old revisions: ", $this->should_delete_old_revisions);

    $this->logging_schema = new CRM_Logging_Schema();

    // Provide partial support for alternate logging database. It needs to
    // be on the same host and use the same username, but can be a different
    // database.
    if (defined('CIVICRM_LOGGING_DSN')) {
      $dsn = DB::parseDSN(CIVICRM_LOGGING_DSN);
      if (!empty($dsn['database'])) {
        $this->logging_database = "`{$dsn['database']}`.";
        $this->debug(1, "Using logging database: ", $this->logging_database);
      }
    }
  }

  /**
   * Make sure logging is set up.
   *
   * Throws an exception if we're unable to generate a unique prefix. You can
   * just try running again - it's just so that it doesn't go into an infinite
   * loop if there really isn't one.
   *
   * @throws CRM_Core_Exception
   */
  public function setup() {
    if ($this->logging_schema->isEnabled()) {
      // Find the date when they started logging. We don't want to create
      // duplicates for log records they already have.
      // This strategy will miss some if they had alternating periods of
      // logging turned on and off, but seems acceptable.
      $this->earliest_logging_date = CRM_Core_DAO::singleValueQuery("SELECT MIN(a.log_date) AS mindate FROM {$this->logging_database}log_civicrm_activity a");
      if (empty($this->earliest_logging_date)) {
        $this->earliest_logging_date = NULL;
      }
      $this->debug(1, "Earliest logging date: ", $this->earliest_logging_date);
    }
    else {
      // If it had never been turned on then the log_XXX tables won't exist,
      // so we need to make sure they do first.
      $this->debug(1, "Enabling trigger-based logging...", "");
      $this->enable_logging();

      // TODO: still thinking about this
      $this->truncate_tables();
    }

    // Unintuitively we need to now turn off logging, otherwise when
    // we clean up the revisions they generate deletion records.
    $this->disable_logging();

    // Turn off revisions!
    Civi::settings()->set('civicaseActivityRevisions', '0');

    $this->conn_id = 0;
    $this->conn_prefix = $this->pick_a_prefix();
    $this->debug(1, "conn_prefix: ", $this->conn_prefix);

    $this->debug(1, "Creating temporary table...", "");
    $this->workaround_dupes_in_civicrm_log();
  }

  /**
   * Do some final tasks.
   */
  public function finish() {
    if ($this->should_delete_old_revisions) {
      $this->delete_old_revisions();
    }

    // Drop our temp table we don't need anymore
    $this->nodupes_table->drop();

    $this->enable_logging();

    $this->debug(0, "Finished.", "");
  }

  /**
   * Do the migration.
   */
  public function run() {
    // Get current revisions
    $current_revision_count = 0;
    $dao = CRM_Core_DAO::executeQuery(
      "SELECT a.*, l.modified_id, l.modified_date as log_modified_date
FROM civicrm_case_activity ca
INNER JOIN civicrm_activity a ON a.id = ca.activity_id
LEFT JOIN `{$this->nodupes_civicrm_log}` l ON (l.entity_table='civicrm_activity' AND l.entity_id=a.id)
WHERE a.is_current_revision = 1
ORDER BY l.modified_date");

    while ($dao->fetch()) {
      $current_revision_count++;
      if ($current_revision_count % 100 == 0) {
        $this->debug(0, "Processed up to {$dao->log_modified_date}. Activity id: ", $dao->id);
      }

      $prior_revision_count = 0;

      if (!empty($dao->original_id)) {
        // Get previous revisions of current activity, ordered by modified date.
        $revision_params = array(
          1 => array($dao->id, 'Integer'),
          2 => array($dao->original_id, 'Integer'),
        );
        // Note the OR in the WHERE is needed because it's possible that
        // an initial revision was created, THEN activity revisions were
        // turned on. In that situation original_id is NULL in the original
        // revision.
        $revision_dao = CRM_Core_DAO::executeQuery(
          "SELECT a.*, l.modified_id, l.modified_date as log_modified_date
FROM civicrm_activity a
LEFT JOIN `{$this->nodupes_civicrm_log}` l ON (l.entity_table='civicrm_activity' AND l.entity_id=a.id)
WHERE (a.id <> %1 AND (a.id = %2 OR a.original_id = %2))
ORDER BY l.modified_date",
          $revision_params);

        $is_first_revision = TRUE;
        while ($revision_dao->fetch()) {
          $prior_revision_count++;
          if ($is_first_revision) {
            $is_first_revision = FALSE;
            if ($this->should_make_record($revision_dao->log_modified_date)) {
              $this->create_record('Insert', $dao->id, $revision_dao->id);
            }
          }
          else {
            if ($this->should_make_record($revision_dao->log_modified_date)) {
              $this->create_record('Update', $dao->id, $revision_dao->id);
            }
          }

          // If you create an activity and then delete it, there should be two
          // records, an insert and a delete. However in the revision system
          // there's only one revision. So do the delete check now in addition to
          // the previous records.
          // Similar when it's an update that is deleted.
          if ($revision_dao->is_deleted == 1) {
            if ($this->should_make_record($revision_dao->log_modified_date)) {
              $this->create_record('Delete', $dao->id, $revision_dao->id);
            }
          }
        }
      }

      // Now do the current revision. If there weren't any earlier revisions,
      // then this is an insert, otherwise an update.
      if ($this->should_make_record($dao->log_modified_date)) {
        if ($prior_revision_count > 0) {
          $this->create_record('Update', $dao->id, $dao->id);
        }
        else {
          $this->create_record('Insert', $dao->id, $dao->id);
        }
        if ($dao->is_deleted == 1) {
          $this->create_record('Delete', $dao->id, $dao->id);
        }
      }
    }

    $this->finish();
  }

  /**
   * Given an activity id, create the corresponding logs.
   *
   * @param string $action What we think the action was, e.g. Update.
   * @param int $id The activity id to use. For all revisions we want
   *   these logging records to pretend it was really updating the same
   *   record, so that it can match how it will work going forward.
   * @param int $revision_id The actual activity id we're working on.
   */
  public function create_record(string $action, int $id, int $revision_id) {
    $this->conn_id++;

    $columns = $this->get_columns($id, 'id', 'civicrm_activity', 'a');

    $params = array(
      1 => array($revision_id, 'Integer'),
      2 => array($action, 'String'),
      3 => array("{$this->conn_prefix}{$this->conn_id}", 'String'),
    );
    CRM_Core_DAO::executeQuery("INSERT INTO {$this->logging_database}log_civicrm_activity SELECT {$columns}, l.modified_date as log_date, %3 AS log_conn_id, l.modified_id AS log_user_id, %2 AS log_action
FROM civicrm_activity a
LEFT JOIN `{$this->nodupes_civicrm_log}` l ON (l.entity_table='civicrm_activity' AND l.entity_id=a.id)
WHERE a.id = %1", $params);

    // For these records, if the activity was deleted these don't get deleted. To properly handle
    // deletion of these we'd need to update the algorithm to compare revisions against each other,
    // e.g. to see if a contact was removed.
    // Left out for now.
    if ($action != 'Delete') {
      $this->create_case_activity_records($action, $id, $revision_id);
      $this->create_contact_records($action, $id, $revision_id);
      $this->create_tag_records($action, $id, $revision_id);
      $this->create_custom_records($action, $id, $revision_id);
    }
  }

  /**
   * Create the related case_activity records.
   *
   * @param string $action What we think the action was, e.g. Update.
   * @param int $id The activity id to use. For all revisions we want
   *   these logging records to pretend it was really updating the same
   *   record, so that it can match how it will work going forward.
   * @param int $revision_id The actual activity id we're working on.
   */
  public function create_case_activity_records(string $action, int $id, int $revision_id) {
    $columns = $this->get_columns($id, 'activity_id', 'civicrm_case_activity', 'ca');

    $params = array(
      1 => array($revision_id, 'Integer'),
      2 => array($action, 'String'),
      3 => array("{$this->conn_prefix}{$this->conn_id}", 'String'),
    );
    CRM_Core_DAO::executeQuery("INSERT INTO {$this->logging_database}log_civicrm_case_activity SELECT {$columns}, l.modified_date as log_date, %3 AS log_conn_id, l.modified_id AS log_user_id, %2 AS log_action
FROM civicrm_case_activity ca
LEFT JOIN `{$this->nodupes_civicrm_log}` l ON (l.entity_table='civicrm_activity' AND l.entity_id=ca.activity_id)
WHERE ca.activity_id = %1", $params);
  }

  /**
   * Create the related activity_contact records.
   *
   * @param string $action What we think the action was, e.g. Update.
   * @param int $id The activity id to use. For all revisions we want
   *   these logging records to pretend it was really updating the same
   *   record, so that it can match how it will work going forward.
   * @param int $revision_id The actual activity id we're working on.
   */
  public function create_contact_records(string $action, int $id, int $revision_id) {
    $columns = $this->get_columns($id, 'activity_id', 'civicrm_activity_contact', 'ac');

    $params = array(
      1 => array($revision_id, 'Integer'),
      2 => array($action, 'String'),
      3 => array("{$this->conn_prefix}{$this->conn_id}", 'String'),
    );
    CRM_Core_DAO::executeQuery("INSERT INTO {$this->logging_database}log_civicrm_activity_contact SELECT {$columns}, l.modified_date as log_date, %3 AS log_conn_id, l.modified_id AS log_user_id, %2 AS log_action
FROM civicrm_activity_contact ac
LEFT JOIN `{$this->nodupes_civicrm_log}` l ON (l.entity_table='civicrm_activity' AND l.entity_id=ac.activity_id)
WHERE ac.activity_id = %1", $params);
  }

  /**
   * Create the related tag records.
   *
   * @param string $action What we think the action was, e.g. Update.
   * @param int $id The activity id to use. For all revisions we want
   *   these logging records to pretend it was really updating the same
   *   record, so that it can match how it will work going forward.
   * @param int $revision_id The actual activity id we're working on.
   */
  public function create_tag_records(string $action, int $id, int $revision_id) {
    $columns = $this->get_columns($id, 'entity_id', 'civicrm_entity_tag', 'et');

    $params = array(
      1 => array($revision_id, 'Integer'),
      2 => array($action, 'String'),
      3 => array("{$this->conn_prefix}{$this->conn_id}", 'String'),
    );
    CRM_Core_DAO::executeQuery("INSERT INTO {$this->logging_database}log_civicrm_entity_tag SELECT {$columns}, l.modified_date as log_date, %3 AS log_conn_id, l.modified_id AS log_user_id, %2 AS log_action
FROM civicrm_entity_tag et
LEFT JOIN `{$this->nodupes_civicrm_log}` l ON (l.entity_table='civicrm_activity' AND l.entity_id=et.entity_id)
WHERE et.entity_id = %1
AND et.entity_table = 'civicrm_activity'", $params);
  }

  /**
   * Create the related custom field records.
   *
   * @param string $action What we think the action was, e.g. Update.
   * @param int $id The activity id to use. For all revisions we want
   *   these logging records to pretend it was really updating the same
   *   record, so that it can match how it will work going forward.
   * @param int $revision_id The actual activity id we're working on.
   * @throws API_Exception
   */
  public function create_custom_records(string $action, int $id, int $revision_id) {
    // Get table names for activity custom fields.
    // Note not catching exception since it means something is weird and
    // skipping over it would just silently miss data. Person has to deal
    // with it.
    $custom_groups = \Civi\Api4\CustomGroup::get()
      ->addSelect('table_name')
      ->addWhere('extends', '=', 'Activity')
      ->setCheckPermissions(FALSE)
      ->execute();

    $params = array(
      1 => array($revision_id, 'Integer'),
    );
    foreach ($custom_groups as $custom_group) {
      $params[2] = array($action, 'String');
      $params[3] = array("{$this->conn_prefix}{$this->conn_id}", 'String');

      $table_name = $custom_group['table_name'];
      $columns = $this->get_columns($id, 'entity_id', $table_name, 'cv');
      CRM_Core_DAO::executeQuery("INSERT INTO {$this->logging_database}log_{$table_name} SELECT {$columns}, l.modified_date as log_date, %3 AS log_conn_id, l.modified_id AS log_user_id, %2 AS log_action
FROM {$table_name} cv
LEFT JOIN `{$this->nodupes_civicrm_log}` l ON (l.entity_table='civicrm_activity' AND l.entity_id=cv.entity_id)
WHERE cv.entity_id = %1", $params);
    }
  }

  public function enable_logging() {
    $this->logging_schema->enableLogging();
    Civi::settings()->set('logging', '1');
  }

  public function disable_logging() {
    $this->logging_schema->disableLogging();
    Civi::settings()->set('logging', '0');
  }

  /**
   * Pick a prefix that doesn't match the first two characters of any existing
   * log_conn_id.
   * @return string
   * @throws CRM_Core_Exception
   */
  private function pick_a_prefix() {
    $num_tries = 0;
    $prefix = NULL;
    do {
      $num_tries++;

      // [A-Z][A-Z]
      $prefix = chr(mt_rand(65, 90)) . chr(mt_rand(65, 90));

      // Check if already exists in db
      $params = array(1 => array($prefix, 'String'));
      $dao = CRM_Core_DAO::executeQuery("SELECT log_conn_id FROM {$this->logging_database}log_civicrm_activity WHERE LEFT(log_conn_id, 2) = %1", $params);
      if ($dao->fetch()) {
        if (!empty($dao->log_conn_id)) {
          // reset so we keep looping
          $prefix = NULL;
        }
      }

    } while ($num_tries < 10 && $prefix === NULL);
    if ($prefix === NULL) {
      throw new CRM_Core_Exception('Unable to generate a unique prefix. Just try running again.', 101);
    }
    return $prefix;
  }

  /**
   * Helper function to decide if the modification was before logging was
   * enabled if logging was already enabled.
   *
   * @param string $modified_date
   *
   * @return bool
   */
  private function should_make_record(string $modified_date) {
    // TODO: These strings are ISO format so comparison works, but technically
    // shouldn't compare as strings.
    return ($this->earliest_logging_date === NULL) || ($modified_date < $this->earliest_logging_date);
  }

  /**
   * There's no real need to keep the old revisions except in case something
   * went wrong. Although during testing you're probably using a copy anyway,
   * so you'd probably also want to test what this does too.
   */
  private function delete_old_revisions() {
    CRM_Core_DAO::executeQuery("DELETE FROM civicrm_activity WHERE is_current_revision = 0");
  }

  /**
   * TODO: still thinking about this.
   * The issue is the initialization records that turning on logging creates.
   * Some are NOT the first revision for any record that already has revisions,
   * and they have the wrong id in terms of what it would have looked like if
   * they had been using trigger-based logging from the start.
   * Further, the feature that they support, namely reverting a record to its
   * state when initially created, is a difficult feature to do properly for
   * non-contact one-to-many records, is probably rarely used, and can create
   * some data inconsistency if you revert something that later had other
   * related changes.
   * Lastly, in a large database you now could have millions of log records
   * that are never used and that slow down reports/changelogs, since the
   * reports have a WHERE clause that excludes them.
   */
  private function truncate_tables() {
    if ($this->earliest_logging_date) {
      // do what exactly?
    }
    else {
      // for each relevant table, if entry is related to a case activity, delete it?
    }
  }

  /**
   * Helper function to get the columns in the table.
   * We want all the columns as-is EXCEPT the id column, since we need it
   * to be the current revision id even if we're logging a past revision,
   * since there is only one id per activity in trigger-based logging.
   *
   * @param int $id The value of the id we want to later insert.
   * @param string $id_column Which column is the actual activity id, e.g.
   *   it's sometimes id, activity_id, entity_id, ...
   * @param string $table The table name.
   * @param string $table_alias The SQL table alias being used by the caller in its SELECT.
   *
   * @return string
   *   A list of columns in a suitable format for use in SQL, where the id_column is our specific id, e.g. "104 AS id, a.name, a.label, etc..."
   */
  private function get_columns(int $id, string $id_column, string $table, string $table_alias) {
    $table_spec = CRM_Caseactivityrevisionmigrator_Schema::columnSpecsOf($table);
    foreach ($table_spec as $column => $dontcare) {
      if ($column == $id_column) {
        $columns[] = "{$id} AS `{$column}`";
      }
      else {
        $columns[] = "{$table_alias}.`{$column}`";
      }
    }
    return implode(', ', $columns);
  }

  /**
   * civicrm_log contains "duplicates" sometimes when case revisions are saved
   * which then create cross-product issues which I was using SELECT DISTINCT
   * to avoid, but that really hurts performance. So we create a mimic table
   * with the duplicates removed.
   * In the current UI you don't see them because it builds an array where
   * the key naturally removes duplicates.
   *
   * Sometimes a single edit via api, like changing just the status,
   * will create two log records that might have a slightly different time,
   * or sometimes the user id is NULL (modified_id) for one of them, e.g. for
   * example if done via `cv` script and you didn't specify a user. We could
   * just say "garbage-in, garbage-out", but it hurts the runtime speed and
   * creates lots of meaningless log_XX table records. In a large database that
   * adds up. So I'm opting to skip them at the expense of possibly losing some
   * information.
   */
  private function workaround_dupes_in_civicrm_log() {
    $this->nodupes_table = CRM_Utils_SQL_TempTable::build()->setDurable()->createWithQuery("
      SELECT DISTINCT l.`entity_table`, l.`entity_id`, l.`modified_id`, l.`modified_date`
      FROM `civicrm_log` l
      WHERE l.`modified_id` IS NOT NULL
      AND (l.`data` IS NULL OR l.`data` NOT LIKE 'Case Activity deleted for source%')
    ");
    $this->nodupes_civicrm_log = $this->nodupes_table->getName();
    CRM_Core_DAO::executeQuery("ALTER TABLE `{$this->nodupes_civicrm_log}` ADD INDEX `index_entity_nodupes` (`entity_table`,`entity_id`)");
  }

  /**
   * Output a log message if level is set to the appropriate level.
   *
   * @param int $level The minimum level at which to output this message.
   * @param string $message
   * @param mixed $variable
   */
  private function debug(int $level, string $message, $variable) {
    if ($this->debug_level >= $level) {
      CRM_Core_Error::debug_log_message("{$message}" . var_export($variable, TRUE));
    }
  }

}
